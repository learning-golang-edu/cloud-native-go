package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"time"
)

func main() {
	pointers()
	arrays()
	slices()
	maps()
	// Control Structures
	forLoops()
	ifStatements()
	switchStatements()

	// Error Handling
	errorHandling()

	// functions
	funWithFunctions()
	// defer
	defer fmt.Printf("cruel world")
	variadicFunctions()
	closures()

	// Structs, Methods, and Interfaces
	structs()
	methods()
	interfaces()

	// Concurrency
	coroutines()
	channels()

	fmt.Printf("goodbye ")
}

func channels() {
	fmt.Println("==== channels ====")
	val := 5
	var chint chan int = make(chan int)
	go func() {
		chint <- val  // Sending on a channel
		val = <-chint // Receiving on a channel and assigning it to val
		<-chint       // Receiving on a channel and discarding the result
	}()

	// channel blocking
	ch := make(chan string) // Allocate a string channel
	go func() {
		message := <-ch      // Blocking receive; assigns to message
		fmt.Println(message) // "ping"
		ch <- "pong"         // Blocking send
	}()
	ch <- "ping"      // Send "ping"
	fmt.Println(<-ch) // "pong"

	// channel buffering
	ch2 := make(chan string, 2) // Buffered channel with capacity 2

	ch2 <- "foo" // Two non-blocking sends
	ch2 <- "bar"

	fmt.Println(<-ch2) // Two non-blocking receives
	fmt.Println(<-ch2)

	//	fmt.Println(<-ch2) // The third receive will block

	// closing channels
	ch3 := make(chan string, 10)

	ch3 <- "foo"

	close(ch3) // One value left in the buffer

	msg, ok := <-ch3
	fmt.Printf("%q, %v\n", msg, ok) // "foo", true

	msg, ok = <-ch3
	fmt.Printf("%q, %v\n", msg, ok) // "", false

	// looping over channels
	ch4 := make(chan string, 3)

	ch4 <- "foo" // Send three (buffered) values to the channel
	ch4 <- "bar"
	ch4 <- "baz"

	close(ch4) // Close the channel

	for s := range ch4 { // Range will continue to the "closed" flag
		fmt.Println(s)
	}

	// select & timeouts
	var ch5 chan int

	select {
	case m := <-ch5: // Read from ch; blocks forever
		fmt.Println(m)

	case <-time.After(5 * time.Second): // time.After returns a channel
		fmt.Println("timed out")
	}
}

func coroutines() {
	fmt.Println("==== coroutines ====")
	foo()    // Call foo() and wait for it to return
	go foo() // Spawn a new goroutine that calls foo() concurrently

	Log(log.Writer(), "hello")
	fmt.Println("world")
	time.Sleep(1 * time.Second)
}

func Log(w io.Writer, message string) {
	go func() {
		_, err := fmt.Fprintln(w, message)
		if err != nil {
			fmt.Println("error caught:", err)
		}
	}() // Don't forget the trailing parentheses!
}

func foo() {
}

func interfaces() {
	fmt.Println("==== Interfaces ====")

	r := Rectangle{Width: 5, Height: 10}
	PrintArea(r)

	c := Circle{Radius: 5}
	PrintArea(c)

	// type assertions
	var s Shape
	s = Circle{}
	c = s.(Circle)
	fmt.Printf("%T\n", c)
}

func PrintArea(s Shape) {
	fmt.Printf("%T's area is %0.2f\n", s, s.Area())
}

type Shape interface {
	Area() float64
}

type Circle struct {
	Radius float64
}

func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

type Rectangle struct {
	Width, Height float64
}

func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

func methods() {
	fmt.Println("==== methods ====")

	vert := &Vertex{3, 4}
	fmt.Println(vert)
	vert.Square()
	fmt.Println(vert)

	mm := MyMap{"A": 1, "B": 2}

	fmt.Println(mm)
	fmt.Println(mm.Length())
}

type MyMap map[string]int

func (m MyMap) Length() int {
	return len(m)
}

func (v *Vertex) Square() {
	v.X *= v.X
	v.Y *= v.Y
}

func structs() {
	fmt.Println("==== structs ====")

	var v Vertex
	fmt.Println(v)

	v = Vertex{}
	fmt.Println(v)

	v = Vertex{1.0, 2.0}
	fmt.Println(v)

	v = Vertex{Y: 2.5}
	fmt.Println(v)

	v = Vertex{X: 1.0, Y: 3.0}
	fmt.Println(v)

	v.X *= 1.5
	v.Y *= 2.5
	fmt.Println(v)

	var vv *Vertex = &Vertex{1, 3}
	fmt.Println(vv)

	vv.X, vv.Y = vv.Y, vv.X
	fmt.Println(vv)
}

type Vertex struct {
	X, Y float64
}

func closures() {
	fmt.Println("==== Anonymous Functions and Closures ====")

	var f func(int, int) int
	f = sum
	fmt.Println(f(3, 5))

	f = product2
	fmt.Println(f(3, 5))

	increment := incrementer()

	fmt.Println(increment())
	fmt.Println(increment())
	fmt.Println(increment())

	newIncrement := incrementer()
	fmt.Println(newIncrement())
}

func incrementer() func() int {
	i := 0

	return func() int {
		i++
		return i
	}
}

func sum(x, y int) int      { return x + y }
func product2(x, y int) int { return x * y }

func variadicFunctions() {
	fmt.Println("==== variadic functions ====")

	const name, age = "Kim", 22
	fmt.Printf("%s is %d years old.\n", name, age)

	fmt.Println(product(2, 2, 2))

	// passing slices as variadic values
	m := []int{3, 3, 3}
	fmt.Println(product(m...))
}

func product(factors ...int) int {
	p := 1
	for _, n := range factors {
		p *= n
	}
	return p
}

func add(x int, y int) int {
	return x + y
}

func foo1(i int, j int, a string, b string) {
	fmt.Println(i, j, a, b)
}
func foo2(i, j int, a, b string) {
	fmt.Println(i, j, a, b)
}

func swap(x, y string) (string, string) {
	return y, x
}

// Recursion
func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}

func closeFile(f *os.File) {
	err := f.Close()
	if err != nil {
		fmt.Println("Error closing file:", err.Error())
	} else {
		fmt.Println("File closed successfully")
	}
}

func zeroByValue(x int) {
	x = 0
}

func zeroByReference(x *int) {
	*x = 0 // Dereference x and set it to 0
}

func funWithFunctions() {
	fmt.Println("==== functions ====")
	sum := add(10, 5)
	fmt.Println(sum)

	foo1(1, 2, "three", "four")
	foo2(1, 2, "three", "four")

	a, b := swap("foo", "bar")
	fmt.Println(a, b)

	// Recursion
	fmt.Println(factorial(11))

	// defer
	file, err := os.Create("/tmp/foo.txt") // Create an empty file
	defer closeFile(file)                  // Ensure closeFile(file) is called
	if err != nil {
		return
	}

	_, err = fmt.Fprintln(file, "Your mother was a hamster")
	if err != nil {
		return
	}
	fmt.Println("File written to successfully")

	// deferred calls are executed in last-in-first-out order
	defer fmt.Println("world")
	defer fmt.Println("cruel")
	defer fmt.Println("goodbye")

	// Pointers as Parameters
	x := 5

	zeroByValue(x)
	fmt.Println(x)

	zeroByReference(&x)
	fmt.Println(x)
}

type NestedError struct {
	Message string
	Err     error
}

func (e *NestedError) Error() string {
	return fmt.Sprintf("%s\n  contains: %s", e.Message, e.Err.Error())
}

func errorHandling() {
	fmt.Println("==== error handling ====")
	_, err := os.Open("somefile.txt")
	if err != nil {
		// log.Fatal(err) // this prints error message and exits
		log.Println(err)
	}

	// Creating an Error
	e1 := errors.New("error 42")
	e2 := fmt.Errorf("error %d", 42)
	log.Println(e1)
	log.Println(e2)

	ne := NestedError{
		Message: "nested",
		Err:     e1,
	}
	log.Println(ne)
}

func switchStatements() {
	fmt.Println("==== switch statements ====")

	i := 0
	switch i % 3 {
	case 0:
		fmt.Println("Zero")
		fallthrough
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	default:
		fmt.Println("Huh?")
	}

	hour := time.Now().Hour()
	switch {
	case hour >= 5 && hour < 9:
		fmt.Println("I'm writing")
	case hour >= 9 && hour < 18:
		fmt.Println("I'm working")
	default:
		fmt.Println("I'm sleeping")
	}

	switch h := time.Now().Hour(); {
	case h >= 5 && h < 9:
		fmt.Println("I'm writing")
	case h >= 9 && h < 18:
		fmt.Println("I'm working")
	default:
		fmt.Println("I'm sleeping")
	}
}

func ifStatements() {
	fmt.Println("==== if statements ====")

	if 7%2 == 0 {
		fmt.Println("7 is even")
	} else {
		fmt.Println("7 is odd")
	}

	if _, err := os.Open("foo.txt"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("All is fine.")
	}
}

func forLoops() {
	fmt.Println("==== for statements ====")

	sum := 0
	for i := 0; i < 10; i++ {
		sum += 1
	}
	fmt.Println(sum)

	// traditional while loop style
	sum, i := 0, 0
	for i < 10 {
		sum += i
		i++
	}
	fmt.Println(i, sum)

	i = 0
	fmt.Println("For ever...")
	// infinite loop
	for {
		fmt.Println("...and ever")
		if i > 10 {
			break
		}
		i++
	}

	// looping over arrays and slices
	s := []int{2, 4, 8, 16, 32}
	for i, v := range s {
		fmt.Printf("%d -> %d\n", i, v)
	}

	a := []int{0, 1, 2, 3, 4}
	sum = 0

	// unneeded return values can be discarded by using the blank identifier underscore operator
	for _, v := range a {
		sum += v
	}

	fmt.Println(sum)

	// looping over maps
	m := map[int]string{
		1: "January",
		2: "February",
		3: "March",
	}

	for k, v := range m {
		fmt.Printf("%d -> %s\n", k, v)
	}
}

func maps() {
	fmt.Println("==== Maps ====")

	freezing := make(map[string]float32)
	freezing["celsius"] = 0.0
	freezing["fahrenheit"] = 32.0
	freezing["kelvin"] = 273.2

	fmt.Println(freezing["kelvin"])
	fmt.Println(len(freezing))

	delete(freezing, "kelvin")
	fmt.Println(len(freezing))

	// maps may also be initialized and populated as map literals
	freezing = map[string]float32{
		"celsius":    0.0,
		"fahrenheit": 32.0,
		"kelvin":     273.2,
	}
	fmt.Println(freezing)

	newton, present := freezing["newton"]
	fmt.Println(newton)
	fmt.Println(present)
}

func slices() {
	fmt.Println("==== Slices ====")

	s0 := []int{0, 1, 2, 3, 4, 5, 6}
	fmt.Println("s0: ", s0)

	s1 := s0[:4]
	fmt.Println("s1: ", s1)

	s2 := s0[3:]
	fmt.Println("s2: ", s2)

	s0[3] = 42
	fmt.Println("s0: ", s0)
	fmt.Println("s1: ", s1)
	fmt.Println("s2: ", s2)
	fmt.Println()

	// working with slices
	var n []int = make([]int, 3)

	fmt.Println(len(n))

	n[0] = 8
	n[1] = 16
	n[2] = 32

	fmt.Println(n)

	// append to the slice
	m := []int{1}

	fmt.Println(m)

	m = append(m, 2)
	m = append(m, 3, 4)
	m = append(m, m...)

	fmt.Println(m)
}

func arrays() {
	fmt.Println("==== Arrays ====")

	var a [3]int = [3]int{2, 4, 6}
	fmt.Println(a)
	fmt.Println(len(a))
	fmt.Println(a[0])
	fmt.Println(a[len(a)-1])

	var b [5]int
	fmt.Println(b)
	fmt.Println(len(b))

	b[2] = 42
	fmt.Println(b)
}

func pointers() {
	fmt.Println("==== Pointers ====")

	var a int = 5

	var p *int = &a
	fmt.Println(p)
	fmt.Println(*p)

	*p = 20
	fmt.Println(a)
	fmt.Println("=====")

	var n *int
	var x, y int

	fmt.Println(n)
	fmt.Println(n == nil)

	fmt.Println(x == y)
	fmt.Println(&x == &x)
	fmt.Println(&x == &y)
	fmt.Println(&x == nil)
}
