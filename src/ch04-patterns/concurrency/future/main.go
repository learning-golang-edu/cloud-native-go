package main

import (
	"ch04-patterns/concurrency"
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()
	future := concurrency.SlowFunction(ctx)

	res, err := future.Result()
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	fmt.Println(res)
}
