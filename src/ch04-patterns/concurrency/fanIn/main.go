package main

import (
	"ch04-patterns/concurrency"
	"fmt"
	"time"
)

func main() {
	sources := make([]<-chan int, 0) // Create an empty channel slice

	for i := 0; i < 3; i++ {
		ch := make(chan int)
		sources = append(sources, ch) // Create a channel; add to sources

		go func() { // Run a toy goroutine for each
			defer close(ch) // Close ch when the routine ends

			for i := 1; i <= 5; i++ {
				ch <- i
				time.Sleep(time.Second)
			}
		}()
	}

	dest := concurrency.Funnel(sources...)
	for d := range dest {
		fmt.Println(d)
	}

	fmt.Println("Done") // Prints only when dest is closed
}
