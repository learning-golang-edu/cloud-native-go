package main

import (
	"ch04-patterns/concurrency"
	"fmt"
)

func main() {
	shardedMap := concurrency.NewShardedMap(5)

	shardedMap.Set("alpha", 1)
	shardedMap.Set("beta", 2)
	shardedMap.Set("gamma", 3)

	fmt.Println(shardedMap.Get("alpha"))
	fmt.Println(shardedMap.Get("beta"))
	fmt.Println(shardedMap.Get("gamma"))

	keys := shardedMap.Keys()
	for _, k := range keys {
		fmt.Println(k)
	}
}
