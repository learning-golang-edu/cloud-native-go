package concurrency

import (
	"crypto/sha1"
	"fmt"
	"sync"
)

type Shard struct {
	sync.RWMutex                        // Compose from sync.RWMutex
	m            map[string]interface{} // m contains the shard's data
}

type ShardedMap []*Shard // ShardedMap is a *Shards slice

func NewShardedMap(nshards int) ShardedMap {
	shards := make([]*Shard, nshards) // Initialize a *Shards slice

	for i := 0; i < nshards; i++ {
		shard := make(map[string]interface{})
		shards[i] = &Shard{m: shard}
	}

	return shards // A ShardedMap IS a *Shards slice!
}

func (m ShardedMap) getShardIndex(key string) int {
	checksum := sha1.Sum([]byte(key)) // Use Sum from "crypto/sha1"
	hash := int(checksum[17])         // Pick a random byte as our hash
	return hash % len(m)              // Mod by len(shards) to get index
}

func (m ShardedMap) getShard(key string) *Shard {
	index := m.getShardIndex(key)
	return m[index]
}

func (m ShardedMap) Delete(key string) {
	shard := m.getShard(key)
	shard.Lock()
	defer shard.Unlock()

	delete(shard.m, key)
}

func (m ShardedMap) Get(key string) interface{} {
	shard := m.getShard(key)
	shard.RLock()
	defer shard.RUnlock()

	return shard.m[key]
}

func (m ShardedMap) Set(key string, value interface{}) {
	shard := m.getShard(key)
	shard.Lock()
	defer shard.Unlock()

	shard.m[key] = value
}

func (m ShardedMap) Keys() []string {
	keys := make([]string, 0) // Create an empty keys slice

	wg := sync.WaitGroup{} // Create a wait group and add a
	wg.Add(len(m))         // wait value for each slice

	for _, shard := range m { // Run a goroutine for each slice
		go func(s *Shard) {
			fmt.Println("shard map", s.m)
			s.RLock()         // Establish a read lock on s
			defer wg.Done()   // Tell the WaitGroup it's done
			defer s.RUnlock() // Release the read lock

			for key := range s.m { // Get the slice's keys
				keys = append(keys, key)
			}
		}(shard)
	}

	wg.Wait() // Block until all reads are done

	return keys // Return combined keys slice
}
