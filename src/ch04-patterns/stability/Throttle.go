package stability

import (
	"context"
	"time"
)

func Throttle(e Effector, max uint, refill uint, d time.Duration) Effector {
	var ticker *time.Ticker = nil
	var tokens = max

	var lastReturnString string
	var lastReturnError error

	return func(ctx context.Context) (string, error) {
		if ctx.Err() != nil {
			return "", ctx.Err()
		}

		if ticker == nil {
			ticker = time.NewTicker(d)
			defer ticker.Stop()

			go func() {
				for {
					select {
					case <-ticker.C:
						t := tokens + refill
						if t > max {
							t = max
						}
						tokens = t
					case <-ctx.Done():
						ticker.Stop()
						break
					}
				}
			}()
		}

		if tokens > 0 {
			tokens--
			lastReturnString, lastReturnError = e(ctx)
		}

		return lastReturnString, lastReturnError
	}
}
