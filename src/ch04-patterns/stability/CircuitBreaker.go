package stability

import (
	"context"
	"errors"
	"time"
)

type Circuit func(ctx context.Context) (string, error)

func Breaker(circuit Circuit, failureThreshold uint64) Circuit {
	var lastStateSuccessful = true
	var consecutiveFailures uint64 = 0
	var lastAttempt = time.Now()

	return func(ctx context.Context) (string, error) {
		if consecutiveFailures >= failureThreshold {
			backoffLevel := consecutiveFailures - failureThreshold
			shouldRetryAt := lastAttempt.Add(time.Second * 2 << backoffLevel)

			if !time.Now().After(shouldRetryAt) {
				return "", errors.New("circuit open -- service unreachable")
			}
		}

		lastAttempt = time.Now()
		response, err := circuit(ctx)

		if err != nil {
			if !lastStateSuccessful {
				consecutiveFailures++
			}
			lastStateSuccessful = false
			return response, err
		}

		lastStateSuccessful = true
		consecutiveFailures = 0

		return response, nil
	}
}
