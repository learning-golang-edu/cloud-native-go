package main

import (
	"ch04-patterns/stability"
	"context"
	"fmt"
	"time"
)

func main() {
	ctx := context.Background()
	ctxt, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	for {
		timeout := stability.Timeout("some input value")
		res, err := timeout(ctxt)
		fmt.Println(res, err)
		if err == nil {
			break
		}
	}
}
