package main

import (
	"ch04-patterns/stability"
	"context"
	"fmt"
	"time"
)

var count int

func myFunc(_ context.Context) (string, error) {
	count++
	return fmt.Sprintf("success %d", count), nil
}

func main() {
	t := stability.Throttle(myFunc, 5, 1, time.Second*2)
	for i := 0; i < 10; i++ {
		res, err := t(context.Background())
		fmt.Println(res, err)
	}
}
