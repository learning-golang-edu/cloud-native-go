package stability

import (
	"context"
	"time"
)

var count = 0

func SlowFunction(arg string) (string, error) {
	count++
	if count%2 != 0 {
		time.Sleep(time.Second * 3)
	}
	return arg, nil
}

func Timeout(arg string) func(context.Context) (string, error) {
	chres := make(chan string)
	cherr := make(chan error)

	go func() {
		res, err := SlowFunction(arg)
		chres <- res
		cherr <- err
	}()

	return func(ctx context.Context) (string, error) {
		select {
		case res := <-chres:
			return res, <-cherr
		case <-ctx.Done():
			return "", ctx.Err()
		}
	}
}
