package main

import (
	"ch04-patterns/stability"
	"context"
	"fmt"
	"log"
	"time"
)

func main() {
	myFunction := func(ctx context.Context) stability.Circuit {
		return func(ctx context.Context) (string, error) {
			return "hello", nil
		}
	}
	ctx := context.Background()
	response, err := stability.Breaker(stability.DebounceFirst(myFunction(ctx), time.Second*1), 5)(ctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(response)

	response, err = stability.Breaker(stability.DebounceLast(myFunction(ctx), time.Second*1), 5)(ctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(response)
}
