package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func HelloMuxHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("Hello gorilla/mux!\n"))
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/", HelloMuxHandler)

	log.Fatal(http.ListenAndServe(":8080", r))
}
