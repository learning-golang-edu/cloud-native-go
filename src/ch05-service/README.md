# Chapter 5: Cloud Native Service

This is simple key-value storage service.

This service stores transaction logs into Postgres database and in order to run this service `docker-compose.yml` file is provided to setup Postgres database.

To start Postgres database execute following command:
```shell
docker-compose up -d
```

## Transport Layer Security

In `gen_certs` folder is an application to generate needed security files:
 - *cert.pem* - certificate file
 - *key.pem* - private key file
